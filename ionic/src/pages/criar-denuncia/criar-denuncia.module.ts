import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CriarDenunciaPage } from './criar-denuncia';

@NgModule({
  declarations: [
    CriarDenunciaPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(CriarDenunciaPage),
  ],
})
export class CriarDenunciaPageModule {}
