import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';

/**
 * Generated class for the PesquisaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pesquisa',
  templateUrl: 'pesquisa.html',
})

export class PesquisaPage {

  pesquisaForm: FormGroup;
  tipos = [
    { id: 1, nome: 'Educaçao', key: 'educacao' },
    { id: 2, nome: 'Infraestrutura', key: 'infraestrutura' },
    { id: 3, nome: 'Meio-ambiente', key: 'meio-ambiente' },
    { id: 4, nome: 'Saúde', key: 'saude' },
    { id: 5, nome: 'Outro', key: 'outro' }
  ];
  statusDenuncia: string = '1';

  constructor(
    private events: Events,
    private fb: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController
  ) {
    const controls = this.tipos.map(c => new FormControl(true));
    this.pesquisaForm = this.fb.group({
      tipos: new FormArray(controls)
    });
  }

  submit() {
    let selectedTipos = this.pesquisaForm.value.tipos
      .map((v, i) => v ? this.tipos[i].key : null)
      .filter(v => v !== null);
    selectedTipos = selectedTipos.join('#');
    this.events.publish('filtro:denuncia',{
      tipoDenuncia: selectedTipos,
      status: +this.statusDenuncia
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesquisaPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
