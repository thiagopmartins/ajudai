import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { Apollo, ApolloModule } from 'apollo-angular';
import { ApolloLink, split } from 'apollo-link';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { onError } from 'apollo-link-error';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { environment } from 'environments/environment.prod';
import { getMainDefinition } from 'apollo-utilities';
import { WebSocketLink } from 'apollo-link-ws'


@NgModule({
    imports: [
        HttpClientModule,
        ApolloModule,
        HttpLinkModule
    ]
})
export class ApolloConfigModule {

    constructor(
        private apollo: Apollo,
        private httpLink: HttpLink
    ) {
        const uri = 'http://localhost:3000/api';
        const http = httpLink.create({ uri });
        const wsLink = new WebSocketLink({
            uri: 'ws://localhost:4000',
            options: {
                reconnect: true
            }
        });
        const link = split(
            ({ query }) => {
                const debug = getMainDefinition(query)
                return debug.kind === 'OperationDefinition' && debug.operation === 'subscription'
            },
            wsLink,
            http
        )
        const linkError = onError(({ graphQLErrors, networkError }) => {
            if (graphQLErrors) {
                graphQLErrors.map(({ message, locations, path }) =>
                    console.log(
                        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
                    ),
                );
            }
            if (networkError) {
                console.log(`[Network error]: ${networkError}`);
            }
        });

        apollo.create({
            link: ApolloLink.from([
                linkError,
                link
            ]),
            cache: new InMemoryCache({
                addTypename: false
            }),
            connectToDevTools: true // !environment.production
        });
    }
}
