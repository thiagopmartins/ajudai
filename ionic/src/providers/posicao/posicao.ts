import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Injectable } from '@angular/core';


@Injectable()
export class PosicaoProvider {

  constructor(
    private geolocation: Geolocation
  ) { }

  getPosicao(): Promise<Geoposition> {
    return new Promise((resolve, reject) => this.geolocation
      .getCurrentPosition()
      .then((minhaPosicao: Geoposition) => {
        resolve(minhaPosicao);
      })
      .catch(err => reject(err)));
  }
}
