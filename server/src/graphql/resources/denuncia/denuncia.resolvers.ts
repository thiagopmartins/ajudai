import * as graphqlFields from 'graphql-fields';
import { GraphQLResolveInfo } from 'graphql';
import { Transaction } from 'sequelize';

import { AuthUser } from './../../../interfaces/AuthUserInterface';
import { authResolvers } from '../../composable/auth.resolver';
import { compose } from '../../composable/composable.resolver';
import { DbConnection } from '../../../interfaces/DbConnectionInterface';
import { DataLoaders } from './../../../interfaces/DataLoadersInterface';
import { handleError, throwError } from './../../../utils/utils';
import { RequestedFields } from './../../ast/RequestedFields';
import { DenunciaInstance } from '../../../models/DenunciaModel';
import { ResolverContext } from '../../../interfaces/ResolverContextInterface';

export const denunciaResolvers = {
    Denuncia: {
        autor: (denuncia, args, { db, dataloaders: { assinaturaLoader } }: { db: DbConnection, dataloaders: DataLoaders }, info: GraphQLResolveInfo) => {
            return db.Assinatura
                .findOne({
                    where: { identidade: denuncia.get('autor')}
                }).catch(handleError);
            /*return assinaturaLoader
                .load({ key: denuncia.get('autor'), info })
                .catch(handleError)*/
        }
    },
    Query: {
        denuncias: (parent, args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Denuncia
                .findAll({
                    attributes: context.requestedfields.getFields(info, { keep: ['id'] })
                }).catch(handleError);
        },
        denunciasOnStatus: (parent, { status }, context: ResolverContext, info: GraphQLResolveInfo) => {
            status = parseInt(status);
            return context.db.Denuncia
                .findAll({
                    where: { status: status },
                    attributes: context.requestedfields.getFields(info, { keep: ['id'] })
                }).catch(handleError);
        },
        denunciasOnTipoAndStatus: (parent, { tipoDenuncia, status }, context: ResolverContext, info: GraphQLResolveInfo) => {
            let tipos = tipoDenuncia.toString().split('#');
            return context.db.Denuncia
                .findAll({
                    where: {
                        tipoDenuncia: { $in: tipos },
                        status: status
                    },
                    attributes: context.requestedfields.getFields(info, { keep: ['id'] })
                }).catch(handleError);
        },
        denunciasOnTipo: (parent, { tipoDenuncia }, context: ResolverContext, info: GraphQLResolveInfo) => {
            let tipos = tipoDenuncia.toString().split('#');
            return context.db.Denuncia
                .findAll({
                    where: { tipoDenuncia: { $in: tipos } },
                    attributes: context.requestedfields.getFields(info, { keep: ['id'] })
                }).catch(handleError);
        },
        denuncia: (parent, { id }, { db, requestedfields }: { db: DbConnection, requestedfields: RequestedFields }, info: GraphQLResolveInfo) => {
            id = parseInt(id);
            return db.Denuncia
                .findById(id, {
                    attributes: requestedfields.getFields(info, { keep: ['id'] })
                })
                .then((denuncia: DenunciaInstance) => {
                    throwError(!denuncia, `Denúncia ID ${id} não existe!`);
                    return denuncia;
                }).catch(handleError);
        }
    },
    Mutation: {
        createDenuncia: compose(...authResolvers)((parent, { input }, { db, authUser }: { db: DbConnection, authUser: AuthUser }, info: GraphQLResolveInfo) => {
            input.autor = authUser.identidade;
            console.log(authUser)
            return db.sequelize.transaction((t: Transaction) => {
                return db.Denuncia
                    .create(input, { transaction: t });
            }).catch(handleError);
        }),
        updateDenuncia: compose(...authResolvers)((parent, { input }, { db, authUser }: { db: DbConnection, authUser: AuthUser }, info: GraphQLResolveInfo) => {
            return db.sequelize.transaction((t: Transaction) => {
                input.id = parseInt(input.id);
                return db.Denuncia
                    .findById(input.id)
                    .then((denuncia: DenunciaInstance) => {
                        throwError(!denuncia, `Denuncia ID ${input.id} não existe!`);
                        return denuncia.update(input, { transaction: t });
                    })
            }).catch(handleError);
        }),
        updateStatusDenuncia:(parent, { input }, { db, pubsub }: { db: DbConnection, pubsub: any }, info: GraphQLResolveInfo) => {
            return db.sequelize.transaction((t: Transaction) => {
                input.id = parseInt(input.id);
                return db.Denuncia
                    .findById(input.id)
                    .then((denuncia: DenunciaInstance) => {
                        throwError(!denuncia, `Denuncia ID ${input.id} não existe!`);
                        pubsub.publish('DENUNCIA_CHANNEL', { atualizaDenuncias: denuncia.id })
                        return denuncia.update({status: input.status}, { transaction: t });
                    })
            }).catch(handleError);
        }
    },
    Subscription: {
        atualizaDenuncias: {
            subscribe: (root, args, { pubsub }) => {
                return pubsub.asyncIterator('DENUNCIA_CHANNEL');
            }
        }
    }
}