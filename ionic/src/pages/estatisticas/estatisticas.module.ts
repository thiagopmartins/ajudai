import { ChartsModule } from 'ng2-charts';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EstatisticasPage } from './estatisticas';

@NgModule({
  declarations: [
    EstatisticasPage,
  ],
  imports: [
    IonicPageModule.forChild(EstatisticasPage),
    ChartsModule
  ],
})
export class EstatisticasPageModule {}
