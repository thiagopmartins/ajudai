import { CriarDenunciaPage } from './../../pages/criar-denuncia/criar-denuncia';
import { Component } from '@angular/core';
import { Events, ModalController } from 'ionic-angular';
import { LatLng } from '@ionic-native/google-maps';
import { PesquisaPage } from './../../pages/pesquisa/pesquisa';

/**
 * Generated class for the MapaBotaoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mapa-botao',
  templateUrl: 'mapa-botao.html'
})
export class MapaBotaoComponent {

  isLocked: boolean = false;

  constructor(
    private events: Events,
    public modalCtrl: ModalController,
  ) {
    this.events.subscribe('locked:button', (locked) => {
      this.isLocked = locked;
    });

    this.events.subscribe('criar:denuncia', (res) => {
      this.criarDenuncia(res);
    });
  }

  pegarPosicao(): boolean {
    (this.isLocked ? this.isLocked = false : this.isLocked = true);
    this.events.publish('posicao:botao', this.isLocked);
    return this.isLocked;
  }

  criarDenuncia(pos?: LatLng): void {
    let modal = this.modalCtrl.create(CriarDenunciaPage, pos);
    modal.present();
    this.events.subscribe('denuncia:criada', () => {
      modal.dismiss();
      //this.events.unsubscribe('denuncia:criada');
    });
  }

  pesquisa(): void {
    let modal = this.modalCtrl.create(PesquisaPage);
    modal.present();
    this.events.subscribe('filtro:denuncia', () => {
      modal.dismiss();
      //this.events.unsubscribe('filtro:denuncia');
    });
  }

}
