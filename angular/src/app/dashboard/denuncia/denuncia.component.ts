import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { Denuncia } from './DenunciaModel';
import { DenunciaService } from './denuncia.service';
import { DialogService } from 'app/dialog.service';


@Component({
  selector: 'app-denuncia',
  templateUrl: './denuncia.component.html',
  styleUrls: ['./denuncia.component.css']
})
export class DenunciaComponent implements OnInit {

  denuncias: Denuncia[];
  denunciaSelecionada: Denuncia;
  basic: boolean;
  descricaoEncoded: string;

  constructor(
    private denunciaService: DenunciaService,
    private dialogService: DialogService,
    private _DomSanitizationService: DomSanitizer
  ) {
    this.denunciaSelecionada = {
      titulo: '',
      descricao: '',
      foto: ''
    }
    this.descricaoEncoded = '';
    this.denunciaService.monitoraDenuncias()
      .subscribe(({ data }) => {
        console.log(data.atualizaDenuncias);
      });
  }

  ngOnInit() {
    this.load();
    console.log(this.denunciaService.monitoraDenuncias());
  }
  load() {
    this.denuncias = [];
    this.denunciaService.todasDenuncias()
      .subscribe((res) => {
        this.denuncias = res
        console.log(this.denuncias);
      });
  }
  onEdit(): void {
    this.basic = true;
    this.descricaoEncoded = atob(this.denunciaSelecionada.descricao);
    console.log(this.denunciaSelecionada);
  }
  aprovar(): void {
    this.denunciaService.atualizarDenuncia({
      id: this.denunciaSelecionada.id,
      status: 1
    })
      .subscribe((res) => {
        if (res.status === 1) {
          this.basic = false;
          this.load();
        }
      });
  }
  rejeitar(): void {
    this.dialogService.confirm(`Deseja rejeitar a denuncia ${this.denunciaSelecionada.titulo} ?`)
      .then((canDelete: boolean) => {
        if (canDelete) {
          this.denunciaService.atualizarDenuncia({
            id: this.denunciaSelecionada.id,
            status: 2
          })
            .subscribe((res) => {
              if (res.status === 2) {
                this.basic = false;
                this.load();
              }
            });
        }
      });

  }
}
