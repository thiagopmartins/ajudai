import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

import { AtualizarComponent } from './atualizar/atualizar';
import { AutenticadorFacebookComponent } from './autenticador-facebook/autenticador-facebook';
import { AutenticadorGoogleComponent } from './autenticador-google/autenticador-google';
import { DenunciaListaComponent } from './denuncia-lista/denuncia-lista';
import { MapaBotaoComponent } from './mapa-botao/mapa-botao';
import { MapaComponent } from './mapa/mapa';

@NgModule({
  imports: [
    CommonModule,
    IonicModule
  ],
  declarations: [
    AtualizarComponent,
    AutenticadorFacebookComponent,
    AutenticadorGoogleComponent,
    DenunciaListaComponent,
    MapaComponent,
    MapaBotaoComponent
  ],
  exports: [
    AtualizarComponent,
    AutenticadorFacebookComponent,
    AutenticadorGoogleComponent,
    DenunciaListaComponent,
    MapaComponent,
    MapaBotaoComponent
  ]
})
export class ComponentsModule { }
