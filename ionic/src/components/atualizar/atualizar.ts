import { Component } from '@angular/core';

/**
 * Generated class for the AtualizarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'atualizar',
  templateUrl: 'atualizar.html'
})
export class AtualizarComponent {

  text: string;

  constructor() {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }


}
