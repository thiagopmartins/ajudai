import { Apollo } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { QUERY_IDENTIDADE_USER, MUTATION_CRIAR_ASSINATURA } from './../../apollo/apollo.service';

@Injectable()
export class IdentidadeProvider {

  constructor(
    private apollo: Apollo
  ) { }

  getIdentidade(identidade: string, nome: string, token: string): Promise<any> {
    return new Promise((resolve, reject) => this.apollo
      .query({
        query: QUERY_IDENTIDADE_USER,
        variables: {
          identidade: identidade
        }
      })
      .toPromise()
      .then(({ data }) => {
        resolve(data['assinatura'].identidade);
      })
      .catch((err) => {
        console.error('Assinatura não encontrada: ', err);
        if(identidade === 'undefined_undefined'){
          reject(err);
        }
        this.apollo
          .mutate({
            mutation: MUTATION_CRIAR_ASSINATURA,
            variables: {
              input: { 
                identidade: identidade,
                nome: nome
              }
            },
            context: {
              headers: new HttpHeaders().set('authorization', `Bearer ${token}`)
            }
          })
          .toPromise()
          .then(({ data }) => resolve(data.createAssinatura.identidade))
          .catch(err => reject(err));
      })
      .catch(err => reject(err)));
  }
}
