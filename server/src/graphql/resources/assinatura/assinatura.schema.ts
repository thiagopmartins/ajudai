const assinaturaTypes = `
    type Assinatura {
        id: ID!
        identidade: String!
        nome: String!
        createdAt: String!
        updatedAt: String!
        denuncias: [ Denuncia! ]!
    }
    input AssinaturaCreateInput {
        identidade: String!
        nome: String!
    }    
`;

const assinaturaQueries = `
    assinaturas: [ Assinatura! ]!
    assinatura(identidade: String!): Assinatura
`;

const assinaturaMutations = `
    createAssinatura(input: AssinaturaCreateInput!): Assinatura
`;

export {
    assinaturaTypes,
    assinaturaQueries,
    assinaturaMutations
}