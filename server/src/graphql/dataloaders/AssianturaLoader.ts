import { AssinaturaInstance, AssinaturaModel } from '../../models/AssinaturaModel';
import { DataLoaderParam } from '../../interfaces/DataLoaderParamInterface';
import { RequestedFields } from './../ast/RequestedFields';

export class AssinaturaLoader {

    static batchAssinaturas(Assinatura: AssinaturaModel, params: DataLoaderParam<string>[], requestedFields: RequestedFields): Promise<AssinaturaInstance[]> {

        let ids: string[] = params.map(param => param.key);
        console.log(ids);
        return Promise.resolve(
            Assinatura.findAll({
                where: { identidade: { $in: ids } },
                attributes: requestedFields.getFields(params[0].info, { keep: ['id'], exclude: ['denuncias'] })
            })
        );
    }

}