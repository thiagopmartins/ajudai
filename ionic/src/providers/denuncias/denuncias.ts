import { LoadingController } from 'ionic-angular';
import { AvisoProvider } from './../aviso/aviso';
import { Apollo } from 'apollo-angular';
import { Denuncia, User } from './../../models/InterfaceModel';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MUTATION_CRIAR_DENUNCIA, QUERY_DENUNCIA_STATUS, MUTATION_ATUALIZAR_DENUNCIA, QUERY_DENUNCIA_TIPO_STATUS, SUBSCRIPTION_DENUNCIA } from './../../apollo/apollo.service';
import { Storage } from '@ionic/storage';
import { Subscription, Observable } from 'rxjs';
import { TEMPO_AVISO_DEFAULT } from '../../utils/utils';
import { map } from 'rxjs/operators';

@Injectable()
export class DenunciasProvider {

  todoSubscription: Subscription;

  constructor(
    private aviso: AvisoProvider,
    private apollo: Apollo,

    private loadingCtrl: LoadingController,
    private storage: Storage
  ) {

  }

  ionViewDidLoad() {
    console.log('TESTE SUBSCRIPTION')
    this.denunciaSubscription();
  }

  criarDenuncia(denuncia: Denuncia) {
    let data = new Date();
    let denunciaKey = `denuncia#${data}`;
    let loading = this.loadingCtrl.create({
      content: 'Enviando sua denúncia...',
      duration: 30000
    });
    loading.present();
    this.salvarDenuncia(denuncia)
      .then(() => {
        console.log('Sua denuncia foi salva com sucesso.');
        this.aviso.sucesso('Sua denúncia foi enviada com sucesso, em breve será analisada por nossa equipe.', TEMPO_AVISO_DEFAULT);
        this.sincronizarDenuncias();
      })
      .catch(err => {
        console.error('Não foi possível gravar sua denuncia: ', err);
        this.storage.set(denunciaKey, denuncia)
          .then(() => {
            console.log('Sua denuncia foi salva localmente para sincronização posterior com os nosso servidores.');
            this.aviso.error('Não foi possível enviar sua denúncia neste momento, tentaremos enviá-la novamente em breve.', TEMPO_AVISO_DEFAULT);
          }).catch(err => console.error('Não foi possível armazenar sua denuncia localmente.', err));
      })
      .then(() => loading.dismiss());
  }

  sincronizarDenuncias() {
    this.storage
      .forEach((value, key) => {
        let denunciaKey = key.split('#')[0];

        if (denunciaKey === 'denuncia') {
          this.salvarDenuncia(value)
            .then(() => {
              console.log('Uma denuncia foi sincronizada com sucesso.');
              this.aviso.sucesso('Sabe aquela denúncia que deu problema antes? Conseguimos enviá-la para nosso servidor.', TEMPO_AVISO_DEFAULT);
              this.storage.remove(key)
                .then(() => console.log('Denuncia removida da base local com sucesso.'))
                .catch((err) => console.error('Não foi possível remover sua denuncia da base local: ', err));
            }).catch(err => console.error('Não foi possível sincronizar com nosso servidor', err));
        }
      })
  }

  salvarDenuncia(denuncia: Denuncia): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get('usuario')
        .then((usuario: User) => {
          console.log(usuario);
          denuncia.status = 0;
          console.log(denuncia);
          this.apollo.
            mutate({
              mutation: MUTATION_CRIAR_DENUNCIA,
              variables: {
                input: denuncia
              },
              context: {
                headers: new HttpHeaders().set('authorization', `Bearer ${usuario.token}`)
              }
            })
            .toPromise()
            .then((res) => {
              resolve(res);
              console.log(res);
            })
            .catch(err => {
              reject(err);
              console.error(err)
            });
        })
    })
  }

  consultarDenunciasOnStatus(status: Number): Observable<Denuncia[]> {
    return this.apollo
      .watchQuery({
        query: QUERY_DENUNCIA_STATUS,
        variables: { status: status }
      }).valueChanges
      .pipe(
        map(res => res.data['denunciasOnStatus'])
      );
  }

  consultarDenunciasOnTipoAndStatus(tipoDenuncia: String, status: Number): Promise<Denuncia[]> {
    console.log(tipoDenuncia, status);
    return new Promise((resolve, reject) => {
      this.apollo
        .query({
          query: QUERY_DENUNCIA_TIPO_STATUS,
          variables: {
            tipoDenuncia: tipoDenuncia,
            status: status
          },
          fetchPolicy: "network-only"
        })
        .toPromise()
        .then(({ data }) => {
          resolve(data['denunciasOnTipoAndStatus'])
        }).catch((err) => reject(err));
    });
  }

  atualizarDenuncia(denuncia: Denuncia): Promise<Denuncia> {
    return new Promise((resolve, reject) => {
      this.storage.get('usuario')
        .then((usuario: User) => {
          this.apollo
            .mutate({
              mutation: MUTATION_ATUALIZAR_DENUNCIA,
              variables: { input: denuncia },
              context: {
                headers: new HttpHeaders().set('authorization', `Bearer ${usuario.token}`)
              }
            })
            .toPromise()
            .then(res => resolve(res))
            .catch(err => reject(err));
        })
    })
  }

  denunciaSubscription() {
    this.todoSubscription = this.apollo
      .subscribe({
        query: SUBSCRIPTION_DENUNCIA
      })
      .subscribe(({ data }) => {
        console.log(data);
      });
  }
}
