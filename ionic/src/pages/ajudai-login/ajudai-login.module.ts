import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AjudaiLoginPage } from './ajudai-login';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AjudaiLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(AjudaiLoginPage),
    ComponentsModule
  ],
})
export class AjudaiLoginPageModule {}
