import { ModelsInterface } from './../interfaces/ModelsInterface';
import * as Sequelize from 'sequelize';

import { BaseModelInterface } from './../interfaces/BaseModelInterface';

import { DenunciaAttributes } from './DenunciaModel';

const base64 = require('base-64');

export interface DenunciaAttributes {
    id?: number;
    titulo?: string;
    descricao?: string;
    tipoDenuncia?: string;
    foto?: string;
    autor?: string;
    status?: number;
    latitude?: number;
    longitude?: number;
    createdAt?: string;
    updatedAt?: string;
}

export interface DenunciaInstance extends Sequelize.Instance<DenunciaAttributes>, DenunciaAttributes { }

export interface DenunciaModel extends BaseModelInterface, Sequelize.Model<DenunciaInstance, DenunciaAttributes> { }

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): DenunciaModel => {
    const Denuncia: DenunciaModel =
        sequelize.define('Denuncia', {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            titulo: {
                type: DataTypes.STRING(128),
                allowNull: false
            },
            descricao: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            tipoDenuncia: {
                type: DataTypes.STRING(128),
                allowNull: false
            },
            foto: {
                type: DataTypes.BLOB({
                    length: 'long'
                })
            },
            latitude: {
                type: DataTypes.FLOAT,
                allowNull: false
            },
            longitude: {
                type: DataTypes.FLOAT,
                allowNull: false
            },
            status: {
                type: DataTypes.INTEGER(2),
                allowNull: false
            }
        }, {
        tableName: 'denuncias',
        hooks: {
            beforeCreate: (denuncia: DenunciaInstance, options: Sequelize.CreateOptions): void => {
                denuncia.descricao = base64.encode(denuncia.descricao);
            }
        }
    });
    Denuncia.associate = (models: ModelsInterface): void => {
        Denuncia.belongsTo(models.Assinatura, {
            targetKey: 'identidade',
            foreignKey: {
                allowNull: false,
                field: 'autor',
                name: 'autor'
            }
        })
    };
    return Denuncia;
}