import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginProvider } from '../../providers/login/login';

@Component({
  selector: 'autenticador-google',
  templateUrl: 'autenticador-google.html'
})
export class AutenticadorGoogleComponent {

  constructor(
    private loginProvider: LoginProvider,
    public navCtrl: NavController,
    public navParams: NavParams
  ) { }

  onLogin() {
    this.loginProvider.loginGoogle()
      .then((user) => {
        console.log(user);
        this.navCtrl.setRoot('MenuPage', user);        
      })
      .catch((err) => console.error(err));
  }

}
