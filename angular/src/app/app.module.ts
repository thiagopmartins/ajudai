import { CoreModule } from './core/core.module';
import { DialogService } from './dialog.service';

import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { DenunciaComponent } from './dashboard/denuncia/denuncia.component';
import { DenunciaService } from './dashboard/denuncia/denuncia.service';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DenunciaComponent,
  ],
  imports: [
    AppRoutingModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DialogService, DenunciaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
