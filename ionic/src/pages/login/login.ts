import { LoginProvider } from './../../providers/login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { User } from '../../models/InterfaceModel';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(
    private loginProvider: LoginProvider,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams
  ) { }

  ionViewDidLoad() {
    if (!this.navParams.data.deslogando || this.navParams.data.deslogando === undefined) {
      this.verificaLoginAutomatico();
    }
  }
  onLogin() {
    this.navCtrl.push('AjudaiLoginPage');
  }

  onRegistro() {
    this.navCtrl.push('RegistroPage');
  }

  verificaLoginAutomatico() {
    console.log('Verificando login automatizado.');
    let loading = this.loadingCtrl.create({
      content: 'Iniciando aplicação...',
      duration: 10000
    });
    loading.present();
    this.loginProvider.loginAutomatico()
      .then((user: User) => {
        if(user !== undefined){
          console.log(user);
          this.navCtrl.setRoot('MenuPage', user);
        }
      })
      .catch((err) => console.error(err))
      .then(() => loading.dismiss());

  }

}
