import { User } from '../../models/InterfaceModel';
import { Apollo } from 'apollo-angular';
import { Injectable } from '@angular/core';

import { MUTATION_CRIAR_USUARIO } from './../../apollo/apollo.service';

@Injectable()
export class RegistroService {

    constructor(
        private apollo: Apollo
    ) { }

    registrar(user: User): Promise<any> {
        return new Promise((resolve, reject) => this.apollo
            .mutate({
                mutation: MUTATION_CRIAR_USUARIO,
                variables: {
                    input: user
                }
            })
            .toPromise()
            .then(id => resolve(id))
            .catch(err => reject(err)));
    }

}