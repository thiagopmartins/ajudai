import { AssinaturaInstance } from './../models/AssinaturaModel';
import * as jwt from 'jsonwebtoken';

import db from './../models';
import { RequestHandler, Request, Response, NextFunction } from "express";
import { JWT_SECRET } from './../utils/utils';

const base64 = require('base-64');

export const extractJwtMiddleware = (): RequestHandler => {

    return (req: Request, res: Response, next: NextFunction): void => {

        let authorization: string = req.get('authorization');
        let token: string = authorization ? authorization.split(' ')[1] : undefined;

        req['context'] = {};
        req['context']['authorization'] = authorization;

        if (!token) { return next(); }

        jwt.verify(token, JWT_SECRET, (err, decoded: any) => {

            if (err) { return next(); }
            const identidade = base64.encode(decoded.sub);
            db.Assinatura.findOne({
                attributes: ['id','identidade'],
                where: { identidade:  identidade},
            }).then((assinatura: AssinaturaInstance) => {

                if (assinatura) {
                    req['context']['authUser'] = {
                        id: assinatura.get('id'),
                        identidade: assinatura.get('identidade')
                    };
                }
                return next();
            });
        });

    };

};