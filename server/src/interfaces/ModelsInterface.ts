import { Denuncia } from './../../../ionic/src/models/InterfaceModel';
import { AssinaturaModel } from './../models/AssinaturaModel';
import { UserModel } from './../models/UserModel';
import { DenunciaModel } from '../models/DenunciaModel';

export interface ModelsInterface {

    Assinatura: AssinaturaModel;
    Denuncia: DenunciaModel;
    User: UserModel;

}