import * as express from 'express';
import * as graphqlHTTP from 'express-graphql';
import * as cors from 'cors';
import * as compression from 'compression';
import * as helmet from 'helmet';
import * as bodyParser from 'body-parser';

import db from './models';
import schema from './graphql/schema';

import { DataLoaderFactory } from './graphql/dataloaders/DataloaderFactory';
import { extractJwtMiddleware } from './middlewares/extract-jwt.middleware';
import { RequestedFields } from './graphql/ast/RequestedFields';

const { GraphQLServer,PubSub } = require('graphql-yoga')

class App {

    public express: express.Application;
    private dataLoaderFactory: DataLoaderFactory;
    private requestedFields: RequestedFields;
    constructor() {
        this.express = express();
        this.init();
    }

    private init(): void {
        this.requestedFields = new RequestedFields();
        this.dataLoaderFactory = new DataLoaderFactory(db, this.requestedFields);
        this.middleware();
    }

    private middleware(): void {

        this.express.use(cors({
            origin: '*', //! SE NECESSÁRIO PODE DEIXAR APENAS UM DOMÍNIO ACESSAR A API. EX: http://teste.com.br
            methods: ['GET', 'POST'],
            allowedHeaders: ['Content-Type', 'Authorization', 'Accept-Enconding'],
            preflightContinue: false,
            optionsSuccessStatus: 204
        }));

        this.express.use(bodyParser.json({limit: '5mb'}));
        this.express.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
        
        this.express.use(compression());

        this.express.use(helmet()); //! SEGURANÇA DA API

        const pubsub = new PubSub();
        const server = new GraphQLServer({ schema: schema, context: { pubsub } })

        server.start(() => console.log('Monitorando alterações localhost:4000'))

        this.express.use('/api',
            extractJwtMiddleware(),
            (req, res, next) => {
                req['context']['db'] = db;
                req['context']['dataloaders'] = this.dataLoaderFactory.getLoaders();
                req['context']['requestedfields'] = this.requestedFields;
                req['context']['pubsub'] = pubsub;
                next();
            },
            graphqlHTTP((req) => ({
                schema: schema,
                graphiql: process.env.NODE_ENV.trim() === 'development',
                context: req['context'],
                
            }))
        );

    }
}

export default new App().express;