const denunciaTypes = `
    type Denuncia {
        id: ID!
        titulo: String!
        descricao: String!
        tipoDenuncia: String!
        foto: String!
        latitude: Float!
        longitude: Float!
        status: Int!
        autor: Assinatura!
        createdAt: String!
        updatedAt: String!
    }
    input DenunciaCreateInput {
        titulo: String!
        descricao: String!
        tipoDenuncia: String!
        foto: String!
        latitude: Float!
        longitude: Float!
        status: Int!
    }
    input DenunciaUpdateInput {
        id: ID!
        titulo: String!
        descricao: String!
        tipoDenuncia: String!
        foto: String!
        latitude: Float!
        longitude: Float!
        status: Int!
    }
    input DenunciaStatusUpdateInput {
        id: ID!
        status: Int!
    }     
`;

const denunciaQueries = `
    denuncias: [ Denuncia! ]!
    denunciasOnStatus(status: Int!): [ Denuncia! ]!
    denunciasOnTipo(tipoDenuncia: String!): [ Denuncia! ]!
    denunciasOnTipoAndStatus(tipoDenuncia: String!, status: Int!): [ Denuncia! ]!
    denuncia(id: ID!): Denuncia
`;

const denunciaMutations = `
    createDenuncia(input: DenunciaCreateInput!): Denuncia
    updateDenuncia(input: DenunciaUpdateInput!): Denuncia
    updateStatusDenuncia(input: DenunciaStatusUpdateInput!): Denuncia
`;

const denunciaSubscriptions = `
    atualizaDenuncias: ID!
`

export {
    denunciaTypes,
    denunciaQueries,
    denunciaMutations,
    denunciaSubscriptions
}