import { MUTATION_ATUALIZAR_DENUNCIA, SUBSCRIPTION_MONITORA_DENUNCIA } from './../../functions.graphql';
import { Apollo } from 'apollo-angular';
import { Denuncia } from './DenunciaModel';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QUERY_TODAS_DENUNCIAS, TodasDenunciasQuery } from 'app/functions.graphql';
import { map } from 'rxjs/operators';

@Injectable()
export class DenunciaService {

    constructor(
        private apollo: Apollo
    ) { }

    todasDenuncias(): Observable<Denuncia[]> {
        return this.apollo
            .query<TodasDenunciasQuery>({
                query: QUERY_TODAS_DENUNCIAS,
                variables: { status: 0 },
                fetchPolicy: 'network-only'
            }).pipe(
                map(res => res.data.denunciasOnStatus)
            );

    }
    monitoraDenuncias() {
        return this.apollo
            .subscribe({
                query: SUBSCRIPTION_MONITORA_DENUNCIA,
            });
    }
    atualizarDenuncia(input): Observable<Denuncia> {
        return this.apollo
            .mutate({
                mutation: MUTATION_ATUALIZAR_DENUNCIA,
                variables: { input: input },
            }).pipe(
                map(res => res.data.updateStatusDenuncia)
            );
    }
}