import { AvisoProvider } from './../../providers/aviso/aviso';
import { DENUNCIA_STATUS, TEMPO_AVISO_DEFAULT } from './../../utils/utils';
import { DenunciasProvider } from './../../providers/denuncias/denuncias';
import { Component } from '@angular/core';
import { Denuncia, User } from './../../models/InterfaceModel';
import { DomSanitizer } from '@angular/platform-browser';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the DetalhesDenunciaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhes-denuncia',
  templateUrl: 'detalhes-denuncia.html',
})

export class DetalhesDenunciaPage {

  denuncia: Denuncia;
  autor: Autor;
  descricaoDecoded: string;
  localDate: string;
  minhaDenuncia: boolean;

  constructor(
    private aviso: AvisoProvider,
    private denunciaProvider: DenunciasProvider,
    private events: Events,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    private _DomSanitizationService: DomSanitizer //! PARA FUNCIONAR A IMAGEM EM BASE 64
  ) {
    this.minhaDenuncia = false;
    this.denuncia = this.navParams.data;
    this.descricaoDecoded = atob(this.denuncia.descricao);
    this.localDate = new Date(this.denuncia.createdAt).toLocaleDateString('pt-BR');
    console.log(this.denuncia);
    this.autor = this.navParams.data.autor;
    this.storage.get('usuario')
      .then((usuario: User) => {
        if(usuario.identidade === this.autor.identidade){
          this.minhaDenuncia = true;
        }
      })
      .catch(err => {
        console.error('Não foi possível encontrar seu usuário: ', err);
        this.navCtrl.setRoot('LoginPage');
      });
  }

  ionViewDidLoad() {
    console.log(this.navParams);
  }
  getTipoDenuncia(tipoDenuncia: string): string {
    let tipo;
    if (tipoDenuncia === 'educacao') {
      tipo = 'Educação';
    } else if (tipoDenuncia === 'infraestrutura') {
      tipo = 'Infraestrutura';
    } else if (tipoDenuncia === 'meio-ambiente') {
      tipo = 'Meio-Ambiente';
    } else if (tipoDenuncia === 'saude') {
      tipo = 'Saúde';
    } else {
      tipo = 'Outro'
    }
    return tipo;
  }

  atualizarDenuncia(){
    delete this.denuncia.createdAt;
    delete this.denuncia['autor'];
    this.denuncia.status = DENUNCIA_STATUS.FINALIZADA;
    this.denunciaProvider.atualizarDenuncia(this.denuncia)
      .then(() => {
        this.navCtrl.pop();
        this.aviso.sucesso('Sua denúncia foi finalizada.', TEMPO_AVISO_DEFAULT);
        this.events.publish('atualizar:denuncia');
      });
  }
}
interface Autor {
  nome?: string;
  identidade?: string;
}
