import gql from 'graphql-tag';

export const MUTATION_CRIAR_USUARIO = gql`
    mutation createUser($input: UserCreateInput!){
        createUser(input: $input){
            id
        }
    }

`;

export const MUTATION_CRIAR_TOKEN = gql`
    mutation createToken($email: String!, $password: String!) {
        createToken(email: $email, password: $password){
            token
        }
    }
`;

export const MUTATION_CRIAR_TOKEN_GOOGLE_FACEBOOK = gql`
    mutation createTokenGoogleFacebook($id: String!) {
        createTokenGoogleFacebook(id: $id){
            token
        }
    }

`;

export const MUTATION_CRIAR_ASSINATURA = gql`
    mutation createAssinatura($input: AssinaturaCreateInput!){
        createAssinatura(input: $input) {
            nome
            identidade
        }
    }

`;

export const MUTATION_CRIAR_DENUNCIA = gql`
    mutation createDenuncia($input: DenunciaCreateInput!){
        createDenuncia(input: $input) {
            titulo
            descricao
            tipoDenuncia
            foto
            latitude
            longitude
            status            
        }
    }
`;

export const MUTATION_ATUALIZAR_DENUNCIA = gql`
    mutation updateDenuncia($input: DenunciaUpdateInput!){
        updateDenuncia(input: $input) {
            id
            titulo
            descricao
            tipoDenuncia
            foto
            latitude
            longitude
            status            
        }
    }
`;

export const QUERY_USER_ON_EMAIL = gql`
    query userOnEmail($email: String!){
        userOnEmail(email: $email) {
            id
            name
            email
        }
    }
`;

export const QUERY_IDENTIDADE_USER = gql`
    query assinatura($identidade: String!){
        assinatura(identidade: $identidade){
            identidade
        }
    }

`;

export const QUERY_DENUNCIA_TIPO_STATUS = gql`
    query denunciasOnTipoAndStatus($tipoDenuncia: String!, $status: Int!){
        denunciasOnTipoAndStatus(tipoDenuncia: $tipoDenuncia, status: $status){
            id
            titulo
            descricao
            tipoDenuncia
            foto
            latitude
            longitude
            status
            createdAt
            autor {
                nome
                identidade
            }            
        }
    }
`;

export const QUERY_DENUNCIA_STATUS = gql`
    query denunciasOnStatus($status: Int!){
        denunciasOnStatus(status: $status){
            id
            titulo
            descricao
            tipoDenuncia
            foto
            latitude
            longitude
            status
            createdAt
            autor {
                nome
                identidade
            }
        }
    }
`;

export const SUBSCRIPTION_DENUNCIA = gql`
    subscription{
        atualizaDenuncias{
            id
            titulo
            descricao
            tipoDenuncia
            foto
            latitude
            longitude
            status
            createdAt
            autor {
                nome
                identidade
            }
        }
    }

`;