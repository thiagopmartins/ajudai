import * as DataLoader from 'dataloader';
import { DataLoaderParam } from './DataLoaderParamInterface';

import { AssinaturaInstance } from './../models/AssinaturaModel';
import { UserInstance } from './../models/UserModel';

export interface DataLoaders {

    assinaturaLoader: DataLoader<DataLoaderParam<string>, AssinaturaInstance>
    userLoader: DataLoader<DataLoaderParam<number>, UserInstance>
    
}