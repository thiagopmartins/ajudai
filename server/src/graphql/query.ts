import { assinaturaQueries } from './resources/assinatura/assinatura.schema';
import { userQueries } from './resources/user/user.schema';
import { denunciaQueries } from './resources/denuncia/denuncia.schema';


const Query = `
    type Query {
        ${assinaturaQueries}
        ${denunciaQueries}
        ${userQueries}
    }
`;

export {
    Query
}