export interface Denuncia {
    id?: number,
    titulo?: string,
    descricao?: string,
    tipoDenuncia?: string,
    foto?: string,
    latitude?: number,
    longitude?: number,
    status?: number
    createdAt?: string;
    updatedAt?: string;
}