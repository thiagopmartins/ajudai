import { Events } from 'ionic-angular';
import { Component } from '@angular/core';

/**
 * Generated class for the DenunciaListaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'denuncia-lista',
  templateUrl: 'denuncia-lista.html'
})
export class DenunciaListaComponent {

  setor: string;

  constructor(
    private events: Events
  ) { }

  selectSetor() {
    this.events.publish('tipo:denuncia', this.setor);
  }
}
