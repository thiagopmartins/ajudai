import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular'
import { AvisoProvider } from '../../providers/aviso/aviso';


@IonicPage()
@Component({
  selector: 'page-denuncia',
  templateUrl: 'denuncia.html',
})
export class DenunciaPage {

  constructor(
    public aviso: AvisoProvider
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad denuncia.')
  }

}
