import { assinaturaMutations } from './resources/assinatura/assinatura.schema';
import { denunciaMutations } from './resources/denuncia/denuncia.schema';
import { tokenMutations } from './resources/token/token.schema';
import { userMutations } from './resources/user/user.schema';

const Mutation = `
    type Mutation {
        ${assinaturaMutations}
        ${denunciaMutations}
        ${tokenMutations}
        ${userMutations}
    }
`;
export {
    Mutation
}