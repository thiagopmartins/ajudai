import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

// Apollo
import { ApolloModule, Apollo } from "apollo-angular";
import ApolloLinkTimeout from 'apollo-link-timeout';
import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { split } from "apollo-link";
import { getMainDefinition } from "apollo-utilities";
import { WebSocketLink } from 'apollo-link-ws'

@NgModule({
    exports: [HttpClientModule, ApolloModule, HttpLinkModule]
})
export class GraphQLModule {
    constructor(apollo: Apollo, httpLink: HttpLink) {
        const timeoutLink = new ApolloLinkTimeout(10000);
        const wsLink = new WebSocketLink({
            uri: 'ws://localhost:4000',
            options: {
                reconnect: true
            }
        });
        const uri = timeoutLink.concat(httpLink.create({ uri: "http://192.168.25.202:3000/api" }));
        const link = split(
            ({ query }) => {
                const { kind, operation } = getMainDefinition(query)
                return kind === 'OperationDefinition' && operation === 'subscription'
            },
            wsLink,
            uri
        )
        //const link = timeoutLink.concat(httpLink.create({ uri: "http://192.168.25.202:3000/api" }));
        console.log('Conectando no apollo...');
        apollo.create({
            link,

            cache: new InMemoryCache({
                addTypename: false
            })
        });
    }
}