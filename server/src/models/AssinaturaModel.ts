import * as Sequelize from 'sequelize';

import { BaseModelInterface } from './../interfaces/BaseModelInterface';

import { AssinaturaAttributes } from './AssinaturaModel';

const base64 = require('base-64');

export interface AssinaturaAttributes {
    id?: number;
    identidade?: string;
    nome?: string;
    createdAt?: string;
    updatedAt?: string;
}

export interface AssinaturaInstance extends Sequelize.Instance<AssinaturaAttributes>, AssinaturaAttributes {
    isIdentidade(encodedIdentidade: string, identidade: string): boolean;
}

export interface AssinaturaModel extends BaseModelInterface, Sequelize.Model<AssinaturaInstance, AssinaturaAttributes> { }

export default (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): AssinaturaModel => {
    const Assinatura: AssinaturaModel =
        sequelize.define('Assinatura', {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            identidade: {
                type: DataTypes.STRING(128),
                allowNull: false,
                unique: true
            },
            nome: {
                type: DataTypes.STRING(128),
                allowNull: false
            }
        }, {
                tableName: 'assinaturas',
                hooks: {
                    beforeCreate: (assinatura: AssinaturaInstance, options: Sequelize.CreateOptions): void => {
                        assinatura.identidade = base64.encode(assinatura.identidade);
                    }
                }
            });
    return Assinatura;
}