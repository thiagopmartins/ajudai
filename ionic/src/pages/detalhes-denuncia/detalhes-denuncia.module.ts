import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesDenunciaPage } from './detalhes-denuncia';

@NgModule({
  declarations: [
    DetalhesDenunciaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesDenunciaPage),
  ],
})
export class DetalhesDenunciaPageModule {}
