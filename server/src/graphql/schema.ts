import { merge } from 'lodash';
import { makeExecutableSchema } from 'graphql-tools';

import { Query } from './query';
import { Mutation } from './mutation';

import { assinaturaResolvers } from './resources/assinatura/assinatura.resolver';
import { tokenResolvers } from './resources/token/token.resolvers';
import { userResolvers } from './resources/user/user.resolvers';

import { assinaturaTypes } from './resources/assinatura/assinatura.schema';
import { denunciaResolvers } from './resources/denuncia/denuncia.resolvers';
import { tokenTypes } from './resources/token/token.schema';
import { userTypes } from './resources/user/user.schema';
import { denunciaTypes } from './resources/denuncia/denuncia.schema';
import { Subscription } from './subscription';


const resolvers = merge(
    assinaturaResolvers,
    denunciaResolvers,
    tokenResolvers,
    userResolvers
);
const SchemaDefinition = `
    type Schema {
        query: Query
        mutation: Mutation
    }

`;

export default makeExecutableSchema({
    typeDefs: [
        assinaturaTypes,
        denunciaTypes,
        Query,
        Mutation,
        SchemaDefinition,
        Subscription,
        tokenTypes,
        userTypes
    ],
    resolvers
});