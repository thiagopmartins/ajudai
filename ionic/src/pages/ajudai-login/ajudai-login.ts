import { TEMPO_AVISO_DEFAULT } from './../../utils/utils';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { AvisoProvider } from './../../providers/aviso/aviso';
import { AjudaiLoginService } from './ajudai-login.service';

@IonicPage()
@Component({
  selector: 'page-ajudai-login',
  templateUrl: 'ajudai-login.html',
})
export class AjudaiLoginPage {

  login: FormGroup;
  loading: any;

  constructor(
    private loginAjudaiService: AjudaiLoginService,
    private aviso: AvisoProvider,
    private fb: FormBuilder,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.login = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AjudaiLoginPage');
  }

  onLogin() {
    this.loading = this.loadingCtrl.create({
      content: 'Conectando com o servidor...',
      duration: 10000
    });
    this.loading.present();
    this.loginAjudaiService.logar(this.login.value)
      .then((user) => {
        console.log(user);
        this.navCtrl.setRoot('MenuPage', user);
      })
      .catch((err: ErrorEventHandler) => {
        console.error(err);
        if(err.toString().includes('Email ou senha estão incorretos')){
          this.aviso.error(`Email ou senha estão incorretos!`, TEMPO_AVISO_DEFAULT);
        }          
      })
      .then(this.loading.dismiss())
  }

}
