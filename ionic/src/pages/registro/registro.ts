import { TEMPO_AVISO_DEFAULT } from './../../utils/utils';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AvisoProvider } from '../../providers/aviso/aviso';
import { RegistroService } from './registro.service';


@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  registro: FormGroup;
  loading: any;

  constructor(
    private alertCtrl: AlertController,
    private aviso: AvisoProvider,
    private fb: FormBuilder,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private registroService: RegistroService
  ) {
    this.registro = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  ionViewDidLoad() {

  }

  onRegistro(): void {
    this.loading = this.loadingCtrl.create({
      content: 'Registrando conta...',
      duration: 10000
    });
    this.loading.present();
    
    this.registroService.registrar(this.registro.value)
      .then((id) => {
        console.log(`Usuário id ${id} criado com sucesso!`);
        this.confirmarAlerta(this.registro.controls['name'].value);       
      })
      .catch((err: ErrorEventHandler) => {
        console.error(err);
        if(err.toString().includes('SequelizeUniqueConstraintError')){
          this.aviso.error(`E-mail ${this.registro.controls['email'].value} já esta cadastrado.`, TEMPO_AVISO_DEFAULT);
          this.registro.controls['email'].setErrors(err);
          this.registro.controls['email'].markAsPristine();
        }     
      })
      .then(() => this.loading.dismiss());
  }

  confirmarAlerta(name: string) {
    const alert = this.alertCtrl.create({
      title: 'Conta registrada!',
      subTitle: `${name} sua conta foi criada, agora você será redirecionado para a tela de login!`,
      buttons: [{
        text: 'Confirmar',
        handler: () => {
          this.navCtrl.setRoot('LoginPage', { deslogando: true })
          this.navCtrl.push('AjudaiLoginPage');
        }
      }]
    }); 
    alert.present();  
  }
}
