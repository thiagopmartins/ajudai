import { denunciaSubscriptions } from "./resources/denuncia/denuncia.schema";

const Subscription = `
    type Subscription {
        ${denunciaSubscriptions}
    }
`;

export {
    Subscription
}