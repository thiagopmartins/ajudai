import { handleError, throwError } from './../../../utils/utils';
import { GraphQLResolveInfo } from "graphql";
import { Transaction } from "sequelize";

import { AssinaturaInstance } from './../../../models/AssinaturaModel';
import { authResolvers } from './../../composable/auth.resolver';
import { AuthUser } from './../../../interfaces/AuthUserInterface';
import { compose } from '../../composable/composable.resolver';
import { DbConnection } from "../../../interfaces/DbConnectionInterface";
import { RequestedFields } from '../../ast/RequestedFields';

const base64 = require('base-64');

export const assinaturaResolvers = {

    Assinatura: {
        denuncias: (assinatura, args, { db, requestedfields }: { db: DbConnection, requestedfields: RequestedFields }, info: GraphQLResolveInfo) => {
            return db.Denuncia
                .findAll({
                    where: { autor: assinatura.get('identidade') },
                    attributes: requestedfields.getFields(info, { keep: ['id'] })
                }).catch(handleError);
        }       
    },

    Query: {

        assinaturas: (parent, args, { db, requestedfields }: { db: DbConnection, requestedfields: RequestedFields }, info: GraphQLResolveInfo) => {
            return db.Assinatura
                .findAll({
                    attributes: requestedfields.getFields(info, { keep: ['id'], exclude: ['denuncias'] })
                }).catch(handleError);
        },

        assinatura: (parent, { identidade }, { db, requestedfields }: { db: DbConnection, requestedfields: RequestedFields }, info: GraphQLResolveInfo) => {
            let lastIdentidade = identidade;
            identidade = base64.encode(identidade);
            return db.Assinatura
                .findOne({
                    where: { identidade: identidade },
                    attributes: requestedfields.getFields(info, { keep: ['id'], exclude: ['denuncias'] })
                })
                .then((assinatura: AssinaturaInstance) => {
                    throwError(!assinatura, `Usuário identidade ${lastIdentidade} não existe!`);
                    return assinatura;
                })
                .catch(handleError);
        }
    },

    Mutation: {

        createAssinatura: compose(...authResolvers)((parent, { input }, { db, authUser }: { db: DbConnection, authUser: AuthUser }, info: GraphQLResolveInfo) => {
            return db.sequelize.transaction((t: Transaction) => {
                return db.Assinatura
                    .create(input, { transaction: t });
            }).catch(handleError);
        })
    }

};