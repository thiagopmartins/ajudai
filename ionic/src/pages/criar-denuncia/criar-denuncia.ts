import { Base64 } from '@ionic-native/base64';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Component, ElementRef, HostListener } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IonicPage, ViewController, Events, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Denuncia } from '../../models/InterfaceModel';
import { DenunciasProvider } from '../../providers/denuncias/denuncias';
import { Geoposition } from '@ionic-native/geolocation';
import { PosicaoProvider } from '../../providers/posicao/posicao';

/**
 * Generated class for the CriarDenunciaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-criar-denuncia',
  templateUrl: 'criar-denuncia.html',
})
export class CriarDenunciaPage {
  @HostListener('input', ['$event.target'])
  onInput(textArea: HTMLTextAreaElement): void {
    this.adjust();
  }

  denuncia: Denuncia;
  denunciaForm: FormGroup;
  descricao: string = '';

  constructor(
    private base64: Base64,
    private camera: Camera,
    private denunciasProvider: DenunciasProvider,
    private element: ElementRef,
    private events: Events,
    private fb: FormBuilder,
    private navParams: NavParams,
    private posicao: PosicaoProvider,
    private viewCtrl: ViewController,
    private _DomSanitizationService: DomSanitizer //! PARA FUNCIONAR A IMAGEM EM BASE 64
  ) {

    this.denunciaForm = this.fb.group({
      titulo: [null, Validators.required],
      descricao: [null, Validators.required]
    });
    this.denuncia = this.denunciaForm.value;
    this.denuncia.foto = '../../assets/imgs/800x600.png';
    if (this.navParams.data[0] === undefined || this.navParams.data[0] === '') {
      this.posicao.getPosicao()
        .then((posicao: Geoposition) => {
          this.denuncia.latitude = posicao.coords.latitude;
          this.denuncia.longitude = posicao.coords.longitude;
        })
        .catch(err => console.error(err));
    } else {
      this.denuncia.latitude = this.navParams.data[0].lat;
      this.denuncia.longitude = this.navParams.data[0].lng;
    }
    this.iniciarCamera();
  }

  ionViewDidLoad() {
    setTimeout(() => this.adjust(), 0);
    this.events.subscribe('tipo:denuncia', (tipoDenuncia) => {
      this.denuncia.tipoDenuncia = tipoDenuncia;
      console.log(this.denuncia);
    });
  }
  adjust(): void {
    let textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
    if (textArea.scrollHeight < 115) {
      textArea.style.overflow = 'hidden';
      textArea.style.height = 'auto';
      textArea.style.height = textArea.scrollHeight + "px";
    } else {
      textArea.style.overflow = 'scroll';
    }
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  iniciarCamera() {
    const cameraOptions: CameraOptions = {
      allowEdit: true,
      cameraDirection: this.camera.Direction.BACK,
      destinationType: this.camera.DestinationType.FILE_URI,
      quality: 70,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 600
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.denuncia.foto = imageData;
      this.base64.encodeFile(this.denuncia.foto).
        then((fotoEncoded: string) => {
          this.denuncia.foto = fotoEncoded;
        })
    }, (err) => {
      console.error('Erro ao capturar a foto ', err);
    });
  }

  onDenuncia() {
    this.denuncia.titulo = this.denunciaForm.controls['titulo'].value;
    this.denuncia.descricao = this.denunciaForm.controls['descricao'].value;
    this.denunciasProvider.criarDenuncia(this.denuncia);
    this.events.publish('denuncia:criada');
  }
}