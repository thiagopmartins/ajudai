import { Apollo } from 'apollo-angular';
import { AvisoProvider } from './../aviso/aviso';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Injectable } from '@angular/core';
import { User } from '../../models/InterfaceModel';

import { MUTATION_CRIAR_TOKEN_GOOGLE_FACEBOOK } from '../../apollo/apollo.service';
import { CONTA, TEMPO_AVISO_DEFAULT } from './../../utils/utils';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {

  user: User;

  constructor(
    private apollo: Apollo,
    private aviso: AvisoProvider,
    private facebook: Facebook,

    private googlePlus: GooglePlus
  ) { }

  loginAutomatico(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.googlePlus.trySilentLogin({ offline: false })
        .then((user) => {
          console.log('Logando no google!');
          this.getToken(`${user.userId}_${CONTA.GOOGLE}`)
            .then(({ data }) => {
              console.log(data.createTokenGoogleFacebook.token);
              user['token'] = data.createTokenGoogleFacebook.token
              resolve(this.deParaGoogle(user));
            })
            .catch((err) => reject(err));
        })
        .catch((err) => {
          console.error('Não foi possível logar automáticamento no google.', err);
          console.log('Logando no facebook!');
          this.facebook.getLoginStatus()
            .then((fbLoginResponse: FacebookLoginResponse) => {
              if (fbLoginResponse.status === 'connected') {
                this.facebook.api(("/me?fields=name,email"), [])
                  .then((user) => {
                    console.log(user);
                    user['accessToken'] = fbLoginResponse.authResponse.accessToken;
                    this.getToken(`${user.id}_${CONTA.FACEBOOK}`)
                      .then(({ data }) => {
                        console.log(data.createTokenGoogleFacebook.token);
                        user['token'] = data.createTokenGoogleFacebook.token
                        resolve(this.deParaFacebook(user));
                      })
                      .catch((err) => reject(err));
                  });
              } else {
                reject('Não foi possível logar automáticamento no facebook');
              }
            })
            .catch((error) => reject(error));
        })
    });
  }

  loginFacebook(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.facebook.login(['public_profile', 'email'])
        .then((fbLoginResponse: FacebookLoginResponse) => {
          if (fbLoginResponse.status === 'connected') {
            this.facebook.api(("/me?fields=name,email"), [])
              .then((user) => {
                console.log(user);
                user['accessToken'] = fbLoginResponse.authResponse.accessToken;
                this.getToken(`${user.id}_${CONTA.FACEBOOK}`)
                  .then(({ data }) => {
                    console.log(data.createTokenGoogleFacebook.token);
                    user['token'] = data.createTokenGoogleFacebook.token
                    resolve(this.deParaFacebook(user));
                  })
                  .catch((err) => reject(err));
              });
          } else {
            reject();
            this.aviso.error('Não foi possível conectar sua conta do facebook.', TEMPO_AVISO_DEFAULT);
          }
        })
        .catch((error) => {
          reject(error);
          this.aviso.error('Ocorreu um erro ao tentar conectar nos servidores do facebook.', TEMPO_AVISO_DEFAULT);
        });
    });
  }

  loginGoogle(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.googlePlus.login({})
        .then((user) => {
          this.getToken(`${user.userId}_${CONTA.GOOGLE}`)
            .then(({ data }) => {
              console.log(data.createTokenGoogleFacebook.token);
              user['token'] = data.createTokenGoogleFacebook.token
              resolve(this.deParaGoogle(user));
            })
            .catch((err) => reject(err));
        }).catch((error) => {
          if (error !== 12501) {
            this.aviso.error(`Ocorreu um erro ao tentar conectar nos servidores da google. CODE: ${error}`, TEMPO_AVISO_DEFAULT);
          }
          reject(error);
        });
    });
  }

  deParaGoogle(usuario): User {
    const usuarioConvertido: User = {};
    usuarioConvertido['accessToken'] = usuario.accessToken;
    usuarioConvertido.token = usuario.token;
    usuarioConvertido.contaTipo = CONTA.GOOGLE;
    usuarioConvertido.name = usuario.displayName;
    usuarioConvertido.email = usuario.email;
    usuarioConvertido.id = usuario.userId;
    usuarioConvertido['foto'] = usuario.imageUrl;
    return usuarioConvertido;
  }

  deParaFacebook(usuario): User {
    const usuarioConvertido: User = {};
    usuarioConvertido['accessToken'] = usuario.accessToken;
    usuarioConvertido.token = usuario.token;
    usuarioConvertido.contaTipo = CONTA.FACEBOOK;
    usuarioConvertido.name = usuario.name;
    usuarioConvertido.email = usuario.email;
    usuarioConvertido.id = usuario.id;
    usuarioConvertido['foto'] = usuario.imageUrl;
    return usuarioConvertido;
  }

  getToken(id: string): Promise<any> {
    return new Promise((resolve, reject) => this.apollo
      .mutate({
        mutation: MUTATION_CRIAR_TOKEN_GOOGLE_FACEBOOK,
        variables: {
          id: id
        }
      })
      .toPromise()
      .then((token) => {
        console.log(token);
        resolve(token);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      }));
  }
}
