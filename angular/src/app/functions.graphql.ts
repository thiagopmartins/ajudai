import gql from 'graphql-tag';
import { Denuncia } from './dashboard/denuncia/DenunciaModel';

export interface TodasDenunciasQuery {
    denunciasOnStatus: Denuncia[];
}

export const QUERY_TODAS_DENUNCIAS = gql`
    query denunciasOnStatus($status: Int!){
        denunciasOnStatus(status: $status) {
            id
            titulo
            descricao
            tipoDenuncia
            foto
            latitude
            longitude
            status
            createdAt
            autor {
                nome
                identidade
            }
        }
    }
`;
export const MUTATION_ATUALIZAR_DENUNCIA = gql`
    mutation updateStatusDenuncia($input: DenunciaStatusUpdateInput!){
        updateStatusDenuncia(input: $input) {
            id
            status
        }
    }
`;
export const SUBSCRIPTION_MONITORA_DENUNCIA = gql`
    subscription atualizaDenuncias{
        atualizaDenuncias
    }
`;