import { IdentidadeProvider } from './../../providers/identidade/identidade';
import { Component, ViewChild } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';
import { IonicPage, NavController, NavParams, Nav, LoadingController } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { Storage } from '@ionic/storage';
import { User } from '../../models/InterfaceModel';

import { CONTA } from '../../utils/utils';
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

export interface PaginaInterface {
  titulo: string;
  nome: string;
  icone?: string;
}

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  rootPage = 'TabsPage';
  user: User = {
    name: 'Usuário Teste'
  };

  @ViewChild(Nav) nav: Nav;

  paginas: PaginaInterface[] = [
    { titulo: 'Home', nome: 'HomePage', icone: 'home' },
    { titulo: 'Sair', nome: '', icone: '' }
  ];

  constructor(
    private facebook: Facebook,
    private googlePlus: GooglePlus,
    private loadingController: LoadingController,
    private identidade: IdentidadeProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage
  ) { }

  abrirPagina(pagina: PaginaInterface) {
    if (pagina.titulo === 'Sair') {
      this.deslogarUsuario();
    } else {
      this.navCtrl.push(pagina.nome);
    }
  }
  ionViewDidLoad() {
    this.user = this.navParams.data;
    console.log('USUARIO: ', this.user);
    this.user.name = this.navParams.data.name;
    this.identidade.getIdentidade(`${this.navParams.data.id}_${this.navParams.data.contaTipo}`, `${this.navParams.data.name}`, `${this.navParams.data.token}`)
      .then((identidade) => {
        console.log(identidade);
        this.navParams.data.identidade = identidade;
        this.storage.set('usuario',this.navParams.data);
      })
      .catch((err) => {
        this.deslogarUsuario();
        this.navCtrl.setRoot('LoginPage', { deslogando: true });
      })
  }
  deslogarUsuario() {
    let loading = this.loadingController.create({
      content: 'Saindo...',
      duration: 10000
    });
    loading.present();
    if (this.getTipoConta() === CONTA.GOOGLE) {
      this.googlePlus.disconnect()
        .then(() => this.navCtrl.setRoot('LoginPage', { deslogando: true }))
        .catch((error) => console.error(error))
        .then(() => loading.dismiss());
    } else if (this.getTipoConta() === CONTA.FACEBOOK) {
      this.facebook.logout()
        .then(() => this.navCtrl.setRoot('LoginPage', { deslogando: true }))
        .catch((error) => console.error(error))
        .then(() => loading.dismiss());
    } else {
      this.navCtrl.setRoot('LoginPage', { deslogando: true });
      loading.dismiss();
    }
    this.storage.remove('usuario');
  }

  getTipoConta(): number {
    return this.user.contaTipo;
  }

}
