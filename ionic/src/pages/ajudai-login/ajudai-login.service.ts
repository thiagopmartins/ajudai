import { MUTATION_CRIAR_TOKEN, QUERY_USER_ON_EMAIL } from './../../apollo/apollo.service';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { HttpHeaders } from '@angular/common/http';
import { User } from '../../models/InterfaceModel';
import { CONTA } from '../../utils/utils';

@Injectable()
export class AjudaiLoginService {

    user: User;

    constructor(
        private apollo: Apollo
    ) { }

    logar(user: User): Promise<string> {
        return new Promise((resolve, reject) => {
            this.apollo
                .mutate({
                    mutation: MUTATION_CRIAR_TOKEN,
                    variables: {
                        email: user.email,
                        password: user.password
                    }
                })
                .toPromise()
                .then(({ data }) => {
                    console.log(data.createToken);
                    
                    this.apollo
                        .query({
                            query: QUERY_USER_ON_EMAIL,
                            context: {
                                headers: new HttpHeaders().set('authorization', `Bearer ${data.createToken.token}`)
                            },
                            fetchPolicy: 'network-only',
                            variables: {
                                email: user.email 
                            }
                        })
                        .toPromise()
                        .then((res) => {
                            console.log(res);
                            let user = res.data['userOnEmail']
                            this.user = {
                                id: user.id,
                                name: user.name,
                                email: user.email,
                                token: data.createToken.token,
                                contaTipo: CONTA.LOCAL
                            }
                            resolve(<string>this.user);
                        })
                        .catch((err: ErrorEventHandler) => reject(err));
                })
                .catch((err: ErrorEventHandler) => reject(err));
        })
    }
}