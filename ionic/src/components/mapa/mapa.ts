import { Denuncia } from './../../models/InterfaceModel';
import { DenunciasProvider } from './../../providers/denuncias/denuncias';
import { TEMPO_AVISO_DEFAULT, DENUNCIA_STATUS } from './../../utils/utils';
import { AvisoProvider } from '../../providers/aviso/aviso';
import { Component } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { LoadingController, Events, NavController } from 'ionic-angular';
import { mapStyle } from './../../theme/map.style';
import {
  GoogleMap,
  GoogleMaps,
  GoogleMapsEvent,
  MyLocation,
  LatLng,
  Marker
} from '@ionic-native/google-maps';
import { Observable } from 'rxjs';
import { SubscriptionResult } from 'apollo-angular';

/**
 * Generated class for the MapaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mapa',
  templateUrl: 'mapa.html'
})
export class MapaComponent {

  map: GoogleMap;
  posicaoError: boolean;
  locked: boolean = false;
  animacao: boolean = false;
  denuncias: Denuncia[];
  marcas: Marker[] = [];

  constructor(
    private aviso: AvisoProvider,
    private denunciaProvider: DenunciasProvider,
    private events: Events,
    private geolocation: Geolocation,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController
  ) {
    console.log('Carregando Mapa!');
    if (window['Cordova']) {
      this.carregarMapa();
    } else {
      console.warn('run browser.');
    }
  }

  carregarMapa() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando Mapa...',
      duration: 15000
    });
    loading.present();
    this.map = GoogleMaps.create('map', {
      styles: mapStyle,
      controls: {
        compass: false,
        mapToolbar: false,
        myLocation: true,
        myLocationButton: false
      }
    });
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        this.map.getMyLocation()
          .then((minhaLocalizacao: MyLocation) => {
            this.setarCamera(minhaLocalizacao.latLng);
          })
          .catch((error) => {
            console.error(error);
            this.aviso.error('Não foi possível buscar sua posição.', TEMPO_AVISO_DEFAULT);
            this.posicaoError = true;
          })
          .then(() => {
            this.consultarDenunciasOnStatus(DENUNCIA_STATUS.APROVADA);
            loading.dismiss();
            this.eventosMapa();
          });
      })
      .catch((error) => {
        console.error('Erro ao carregar o mapa', error);
        loading.dismiss();
        this.aviso.error('Erro ao carregar o mapa!', TEMPO_AVISO_DEFAULT);
      });
  }
  consultarDenunciasOnTipoAndStatus(tipoDenuncia: String, status: Number) {
    this.denunciaProvider.consultarDenunciasOnTipoAndStatus(tipoDenuncia, status)
      .then((res: Denuncia[]) => {
        this.denuncias = res;
        console.log(res);
        console.log(this.denuncias);
        this.criarMarca(this.denuncias);
      }).catch((err) => console.error(err));
  }  
  consultarDenunciasOnStatus(status: Number) {
    this.denunciaProvider.consultarDenunciasOnStatus(status)
      .toPromise()
      .then((res: Denuncia[]) => {
        this.denuncias = res;
        console.log(res);
        console.log(this.denuncias);
        this.criarMarca(this.denuncias);
      }).catch((err) => console.error(err));
  }
  eventosMapa() {
    console.log('Monitorando os eventos do mapa!');

    this.map.on(GoogleMapsEvent.MAP_LONG_CLICK).subscribe((res: LatLng) => {
      this.events.publish('criar:denuncia', (res));
    });

    this.events.subscribe('posicao:botao', (locked) => {
      this.locked = locked;
      if (this.locked) {
        this.geolocation.getCurrentPosition()
          .then((minhaPosicao: Geoposition) => {
            this.map.animateCamera({
              target: {
                lat: minhaPosicao.coords.latitude,
                lng: minhaPosicao.coords.longitude
              }
            });
          })
          .catch((error) => console.error(error));
      }
    });

    this.events.subscribe('atualizar:denuncia', () => {
      this.denuncias = [];
      this.consultarDenunciasOnStatus(DENUNCIA_STATUS.APROVADA);
    });

    this.events.subscribe('filtro:denuncia', (input) => {
      console.log(input);
      this.denuncias = [];
      this.consultarDenunciasOnTipoAndStatus(input.tipoDenuncia, input.status);
    });

    this.geolocation.watchPosition()
      .subscribe((minhaPosicao: Geoposition) => {
        if (this.posicaoError == true) {
          this.posicaoError = false;
          this.setarCamera(<LatLng>{ lat: minhaPosicao.coords.latitude, lng: minhaPosicao.coords.longitude });
        } else {
          if (this.locked) {
            this.animacao = true;
            this.map.animateCamera({
              target: {
                lat: minhaPosicao.coords.latitude,
                lng: minhaPosicao.coords.longitude
              }
            }).then(() => this.animacao = false);
          }
        }
      });
  }
  setarCamera(latLng: LatLng): void {
    this.map.setOptions({
      camera: {
        target: latLng,
        zoom: 18,
        tilt: 22
      }
    });
  }

  criarMarca(denuncias: Denuncia[]) {
    this.map.clear();
    denuncias.forEach((denuncia: Denuncia, index: Number) => {
      this.marcas[+index] = this.map.addMarkerSync({
        title: `${denuncia.titulo} #${denuncia.id}`,
        icon: this.iconeCor(denuncia.tipoDenuncia),
        animation: 'DROP',
        position: {
          lat: denuncia.latitude,
          lng: denuncia.longitude
        },
        snippet: `Criada por: ${denuncia['autor'].nome}\nData: ${new Date(denuncia['createdAt']).toLocaleDateString('pt-BR')}\nClique aqui para mais detalhes.`
      });
      this.detalhesDenunciaEvento(this.marcas[+index]);
    });
  }

  detalhesDenunciaEvento(marca: Marker) {
    marca.on(GoogleMapsEvent.INFO_CLICK).subscribe((marcaClick) => {
      let denunciaId = marcaClick[1].getTitle().split('#')[1].trim();
      this.denuncias.forEach((denuncia: Denuncia) => {      
        if (denunciaId === denuncia.id) {         
          this.navCtrl.push('DetalhesDenunciaPage', denuncia)
        }
      })
    });
  }

  iconeCor(tipo: String): String {
    let cor = '';
    if (tipo === 'educacao') {
      cor = 'blue';
    } else if (tipo === 'infraestrutura') {
      cor = 'yellow';
    } else if (tipo === 'meio-ambiente') {
      cor = 'green';
    } else if (tipo === 'saude') {
      cor = 'red';
    } else {
      cor = 'purple'
    }
    return cor;
  }
}