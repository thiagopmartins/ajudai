import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DenunciaPage } from './denuncia';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DenunciaPage,
  ],
  imports: [
    IonicPageModule.forChild(DenunciaPage),
    ComponentsModule
  ]
})
export class DenunciaPageModule {}
