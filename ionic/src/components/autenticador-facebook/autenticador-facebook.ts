import { NavController } from 'ionic-angular';
import { Component } from '@angular/core';
import { LoginProvider } from '../../providers/login/login';
import { User } from '../../models/InterfaceModel';
/**
 * Generated class for the FacebookComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'autenticador-facebook',
  templateUrl: 'autenticador-facebook.html'
})
export class AutenticadorFacebookComponent {

  text: string;

  constructor(
    private loginProvider: LoginProvider,
    private navCtrl: NavController
  ) { }
  onLogin() {

    this.loginProvider.loginFacebook()
      .then((user: User) => {
        console.log(user);
        this.navCtrl.setRoot('MenuPage', user);
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
