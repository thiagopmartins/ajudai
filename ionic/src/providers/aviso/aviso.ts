import { Injectable } from '@angular/core';
import { Toast, ToastController } from 'ionic-angular';

/*
  Generated class for the AvisoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AvisoProvider {

  toast: Toast;

  constructor(
    private toastCtrl: ToastController
  ) { }

  error(mensagem: string, tempo: number): void {
    console.log(mensagem);
    this._enviar(mensagem, 'error', tempo);
  }

  sucesso(mensagem: string, tempo: number): void {
    console.log(mensagem);
    this._enviar(mensagem, 'sucesso', tempo, false)
  } 

  private _enviar(mensagem: string, tipo: string, tempo: number, dismiss?: boolean): void {
    if (this.toast !== undefined) {
      this.toast.dismiss();
    }
    if(dismiss === undefined)
      dismiss = true;
    this.toast = this.toastCtrl.create({
      message: mensagem,
      duration: tempo,
      position: 'top',
      showCloseButton: true,
      cssClass: tipo,
      dismissOnPageChange: dismiss

    });
    this.toast.present();
  }

}
