import { DataLoaderParam } from './../../interfaces/DataLoaderParamInterface';
import { RequestedFields } from './../ast/RequestedFields';
import * as DataLoader from 'dataloader';

import { AssinaturaInstance } from './../../models/AssinaturaModel';
import { AssinaturaLoader } from './AssianturaLoader';
import { DbConnection } from "../../interfaces/DbConnectionInterface";
import { DataLoaders } from "../../interfaces/DataLoadersInterface";
import { UserInstance } from './../../models/UserModel';
import { UserLoader } from './UserLoader';

export class DataLoaderFactory {

    constructor(
        private db: DbConnection,
        private requestedFields: RequestedFields
    ) { }

    getLoaders(): DataLoaders {

        return {
            userLoader: new DataLoader<DataLoaderParam<number>, UserInstance>(
                (params: DataLoaderParam<number>[]) => UserLoader.batchUsers(this.db.User, params, this.requestedFields),
                { cacheKeyFn: (param: DataLoaderParam<number[]>) => param.key }
            ),
            assinaturaLoader: new DataLoader<DataLoaderParam<string>, AssinaturaInstance>(
                (params: DataLoaderParam<string>[]) => AssinaturaLoader.batchAssinaturas(this.db.Assinatura, params, this.requestedFields),
                { cacheKeyFn: (param: DataLoaderParam<string[]>) => param.key }
            )            
        };
    }

}