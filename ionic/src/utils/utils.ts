export enum CONTA {
    LOCAL = 0,
    GOOGLE = 1,
    FACEBOOK = 2
};
export enum DENUNCIA_STATUS {
    ANALISE = 0,
    APROVADA = 1,
    FINALIZADA = 2,
    REPROVADA = 3
};
export const TEMPO_AVISO_DEFAULT = 10000;