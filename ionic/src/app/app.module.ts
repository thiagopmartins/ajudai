import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, LoadingController, ToastController } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';

import { Base64 } from '@ionic-native/base64';
import { Camera } from '@ionic-native/camera';
import { ChartsModule } from 'ng2-charts';
import { Facebook } from '@ionic-native/facebook';
import { Geolocation } from '@ionic-native/geolocation';
import { GooglePlus } from '@ionic-native/google-plus';
import { Network } from '@ionic-native/network';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AjudaiLoginPageModule } from './../pages/ajudai-login/ajudai-login.module';
import { AjudaiLoginService } from './../pages/ajudai-login/ajudai-login.service';
import { AnuncioPageModule } from '../pages/anuncio/anuncio.module';
import { AvisoProvider } from '../providers/aviso/aviso';
import { ComponentsModule } from './../components/components.module';
import { CriarDenunciaPageModule } from '../pages/criar-denuncia/criar-denuncia.module';
import { DenunciaPageModule } from '../pages/denuncia/denuncia.module';
import { DenunciasProvider } from '../providers/denuncias/denuncias';
import { DetalhesDenunciaPageModule } from '../pages/detalhes-denuncia/detalhes-denuncia.module';
import { EstatisticasPageModule } from '../pages/estatisticas/estatisticas.module';
import { GraphQLModule } from './../apollo/apollo.module';
import { HomePageModule } from '../pages/home/home.module';
import { IdentidadeProvider } from '../providers/identidade/identidade';
import { LoginPageModule } from '../pages/login/login.module';
import { LoginProvider } from '../providers/login/login';
import { PesquisaPageModule } from '../pages/pesquisa/pesquisa.module';
import { PosicaoProvider } from '../providers/posicao/posicao';
import { MenuPageModule } from '../pages/menu/menu.module';
import { RegistroService } from './../pages/registro/registro.service';
import { TabsPageModule } from '../pages/tabs/tabs.module';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(
      {
        name:'dbAjudai',
        driverOrder: ['indexeddb', 'sqlite', 'websql'],
        storeName:'ajudai'        
      }      
    ),
    AjudaiLoginPageModule,
    AnuncioPageModule,
    BrowserModule,
    ChartsModule,
    CriarDenunciaPageModule,
    ComponentsModule,
    DenunciaPageModule,
    DetalhesDenunciaPageModule,
    EstatisticasPageModule,
    GraphQLModule,
    HomePageModule,
    LoginPageModule,
    MenuPageModule,
    PesquisaPageModule,
    TabsPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    AjudaiLoginService,
    AvisoProvider,
    Base64,
    Camera,
    DenunciasProvider,
    Facebook,
    Geolocation,
    GooglePlus,
    IdentidadeProvider,
    LoginProvider,
    LoadingController,
    LoginProvider,
    PosicaoProvider,
    Network,
    SplashScreen,
    StatusBar,
    Storage,
    RegistroService,
    ToastController,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ]
})
export class AppModule { }
