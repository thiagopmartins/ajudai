import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { DenunciaComponent } from './dashboard/denuncia/denuncia.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard/denuncia',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        children: [
            {
                path: '',
                redirectTo: '/dashboard/denuncia',
                pathMatch: 'full'
            },
            {
                path: 'denuncia',
                component: DenunciaComponent,
                data: {
                    title: 'Monitoramento de denuncias'
                }
            }
        ]
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { useHash: true })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
